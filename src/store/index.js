import api from '../../api/products.js';
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  // we need to call mutation and set products to store, not directly return them from action
  actions: {
    getProductsList(ctx, data) {
      return api.getProductsList();
    },
  },
  modules: {},
});
